﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtmChainOfResponsibility
{
    public interface ITarget
    {
        bool Validate(string str);
        string Withdraw(string str);
    }

    class AtmAdapter : ITarget
    {
        private Atm.Bancomat atm = new Atm.Bancomat();

        public bool Validate(string str)
        {
            return atm.Validate(str);
        }
        /// <summary>
        /// Format: "[value] {рублей/$/euro}"
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string Withdraw(string str)
        {
            var money = new MoneyParser();
            return atm.Withdraw((SomeMoney)money.Parse(str));
        }
    }
}
