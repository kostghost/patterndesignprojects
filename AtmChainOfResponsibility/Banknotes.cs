﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtmChainOfResponsibility
{
    public enum CurrencyType
    {
        Eur,
        Dollar,
        Ruble
    }

    public interface IBanknote
    {
        CurrencyType Currency { get; }
        int Value { get; }
    }

    public class SomeMoney : IBanknote
    {
        public CurrencyType Currency { get; set; }
        public int Value { get; set; }

        public SomeMoney(CurrencyType currency, int value)
        {
            Currency = currency;
            Value = value;
        }

        //ООоо, пошли костыли
        public static explicit operator SomeMoney(MoneyParser v)
        {
            return new SomeMoney(v.Currency, v.Value);
        }


    }
    /* Не надо
    public class BaseBanknoteRub : IBanknote
    {
        public virtual CurrencyType Currency { get; }
        public virtual int Value { get; set; }

        public BaseBanknoteRub(int value)
        {
            Currency = CurrencyType.Ruble;
        }
    }

    public class BanknoteRub10 : BaseBanknoteRub
    {
        public BanknoteRub10(int value) : base(value)
        {
            Value = 10;
        }
    }

    public class BanknoteRub50 : BaseBanknoteRub
    {
        public BanknoteRub50(int value) : base(value)
        {
            Value = 50;
        }
    }

    public class BanknoteRub100 : BaseBanknoteRub
    {
        public BanknoteRub100(int value) : base(value)
        {
            Value = 100;
        }
    }

    public class BanknoteRub500 : BaseBanknoteRub
    {
        public BanknoteRub500(int value) : base(value)
        {
            Value = 500;
        }
    }

    public class BanknoteRub1000 : BaseBanknoteRub
    {
        public BanknoteRub1000(int value) : base(value)
        {
            Value = 1000;
        }
    }
    */
}
