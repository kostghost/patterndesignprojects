﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace AtmChainOfResponsibility
{
    class Program
    {

        static void DisplayWithdraw(AtmAdapter atm ,string str)
        {
            Console.WriteLine(str + " = " + atm.Withdraw(str));
        }

        static void Main(string[] args)
        {
            var atm = new AtmAdapter();
            Console.WriteLine(atm.Validate("10 $"));
            Console.WriteLine(atm.Validate("10 рублей"));
            Console.WriteLine(atm.Validate("20 рублей"));

            DisplayWithdraw(atm, "6120 рублей");
            DisplayWithdraw(atm, "2123 рублей");
            DisplayWithdraw(atm, "660 $");
            DisplayWithdraw(atm, "247 $");

            Console.Read();
        }
    }
}
