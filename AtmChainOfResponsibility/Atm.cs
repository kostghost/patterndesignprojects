﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace AtmChainOfResponsibility
{
    class Atm
    {
        public class Bancomat
        {
            private readonly BanknoteHandler _handler;

            public Bancomat()
            {
                //Задаем в обратном порядке!
                _handler = new TenRubleHandler(null);
                _handler = new FiftyRubleHandler(_handler);
                _handler = new HundredRubleHandler(_handler);
                _handler = new FiveHundredRubleHandler(_handler);
                _handler = new ThousandRubleHandler(_handler);
                _handler = new TenDollarHandler(_handler);
                _handler = new FiftyDollarHandler(_handler);
                _handler = new HundredDollarHandler(_handler);

            }

            public bool Validate(string banknote)
            {
                return _handler.Validate(banknote);
            }

            public string Withdraw(SomeMoney money)
            {
                return _handler.Withdraw(money);
            }
        }

        public abstract class BanknoteHandler
        {
            private readonly BanknoteHandler _nextHandler;

            protected BanknoteHandler(BanknoteHandler nextHandler)
            {
                _nextHandler = nextHandler;
            }
            
            //Спорно, порождаем костыли.
            protected static string MoneyToWithdraw { get; set; }


            public virtual bool Validate(string banknote)
            {
                return _nextHandler != null && _nextHandler.Validate(banknote);
            }

            public virtual string Withdraw(SomeMoney money)
            {
                return  _nextHandler.Withdraw(money);
            }
        }


        #region ruble
        public abstract class RubleHandlerBase : BanknoteHandler
        {
            public override bool Validate(string banknote)
            {
                if (banknote.Equals($"{Value} рублей"))
                {
                    return true;
                }
                return base.Validate(banknote);
            }

            //Нужно обернуть бы,чтобы не повторяться для каждого Currency, не придумал как
            public override string Withdraw(SomeMoney money)
            {
                if (money.Currency == CurrencyType.Ruble && money.Value - Value >= 0)
                {
                    int valueGetTimes = money.Value / Value;

                    //остаток
                    int rest = money.Value - valueGetTimes * Value; //это работает?

                    if (MoneyToWithdraw != null)
                        MoneyToWithdraw += " + ";
                    MoneyToWithdraw += Value + "x" + valueGetTimes;

                    if (Value == 10) //Вот это бы засунуть в обработчик 10 рублей, но вот как в таком случае написать?
                    {
                        //Костыли!!!
                        string result;
                        if (rest != 0)
                            result = MoneyToWithdraw + " + " + rest + " не валидная сумма :C";
                        else
                            result = MoneyToWithdraw;
                        MoneyToWithdraw = null;
                        return result;
                    }

                    else
                        return base.Withdraw(new SomeMoney(money.Currency, rest));
                }

                return base.Withdraw(money);
            }

            protected abstract int Value { get; }

            protected RubleHandlerBase(BanknoteHandler nextHandler) : base(nextHandler)
            {
            }
        }

        public class TenRubleHandler : RubleHandlerBase
        {
            protected override int Value => 10;

            public TenRubleHandler(BanknoteHandler nextHandler) : base(nextHandler)
            { }
        }
        public class FiftyRubleHandler : RubleHandlerBase
        {
            protected override int Value => 50;

            public FiftyRubleHandler(BanknoteHandler nextHandler) : base(nextHandler)
            { }
        }
        public class HundredRubleHandler : RubleHandlerBase
        {
            protected override int Value => 100;

            public HundredRubleHandler(BanknoteHandler nextHandler) : base(nextHandler)
            { }
        }
        public class FiveHundredRubleHandler : RubleHandlerBase
        {
            protected override int Value => 500;

            public FiveHundredRubleHandler(BanknoteHandler nextHandler) : base(nextHandler)
            { }
        }
        public class ThousandRubleHandler : RubleHandlerBase
        {
            protected override int Value => 1000;

            public ThousandRubleHandler(BanknoteHandler nextHandler) : base(nextHandler)
            { }
        }

        #endregion


        #region dollar
        public abstract class DollarHandlerBase : BanknoteHandler
        {
            public override bool Validate(string banknote)
            {
                if (banknote.Equals($"{Value} $"))
                {
                    return true;
                }
                return base.Validate(banknote);
            }

            //Нужно обернуть бы,чтобы не повторяться для каждого Currency, не придумал как
            public override string Withdraw(SomeMoney money)
            {
                if (money.Currency == CurrencyType.Dollar && money.Value - Value >= 0)
                {
                    int valueGetTimes = money.Value / Value;
                    
                    //остаток
                    int rest =  money.Value - valueGetTimes*Value; //это работает?

                    if (MoneyToWithdraw != null)
                        MoneyToWithdraw += " + ";
                    MoneyToWithdraw += Value + "x" + valueGetTimes;

                    if (Value == 10) //Вот это бы засунуть в обработчик 10 долларов, но вот как в таком случае написать?
                    {
                        //Костыли!!!
                        string result;
                        if (rest != 0)
                            result = MoneyToWithdraw + " + " + rest + " не валидная сумма :C";
                        else
                            result = MoneyToWithdraw;
                        MoneyToWithdraw = null;
                        return result;
                    }
                        
                    else
                        return base.Withdraw(new SomeMoney(money.Currency, rest));
                }

                return base.Withdraw(money);
            }

            protected abstract int Value { get; }

            protected DollarHandlerBase(BanknoteHandler nextHandler) : base(nextHandler)
            {
            }
        }

        public class HundredDollarHandler : DollarHandlerBase
        {
            protected override int Value => 100;

            public HundredDollarHandler(BanknoteHandler nextHandler) : base(nextHandler)
            { }
        }
        public class FiftyDollarHandler : DollarHandlerBase
        {
            protected override int Value => 50;

            public FiftyDollarHandler(BanknoteHandler nextHandler) : base(nextHandler)
            { }
        }
        public class TenDollarHandler : DollarHandlerBase
        {
            protected override int Value => 10;

            public TenDollarHandler(BanknoteHandler nextHandler) : base(nextHandler)
            { }
        }

        #endregion
    }
}

