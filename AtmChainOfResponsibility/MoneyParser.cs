﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtmChainOfResponsibility
{
    public class MoneyParser : IBanknote
    {
        public CurrencyType Currency { get; set; }
        public int Value { get; set; }

        /// <summary>
        /// 
        /// Format: "[value] {рублей/$/euro}"
        /// </summary>
        /// <param name="str"></param>
        public MoneyParser Parse(string str)
        {
            //Без проверок
            var splitStr = str.Split(' ');
            Value = int.Parse(splitStr[0]);

            //Опасно!
            if (splitStr[1].Trim() == "рублей")
                Currency = CurrencyType.Ruble;
            else if (splitStr[1].Trim() == "$")
                Currency = CurrencyType.Dollar;
            else
                Currency = CurrencyType.Eur;

            return this;
        }
    }
}
