﻿using Xrm.ReportUtility.Interfaces;
using Xrm.ReportUtility.Models;

namespace Xrm.ReportUtility.Infrastructure.Transformers.Abstract
{

    //Использован паттерн Декоратор, а также шаблонный метод (ReportServiceTransformerBase Является шаблонным классом)
    //Decorator - ReportServiceTransformerBase 
    //ConcreteDecorator - CostSumReportTransformer, CountSumReportTransformer, ...
    public abstract class ReportServiceTransformerBase : IDataTransformer
    {
        protected readonly IDataTransformer DataTransformer;

        protected ReportServiceTransformerBase(IDataTransformer dataTransformer)
        {
            DataTransformer = dataTransformer;
        }

        public abstract Report TransformData(DataRow[] data);
    }
}
