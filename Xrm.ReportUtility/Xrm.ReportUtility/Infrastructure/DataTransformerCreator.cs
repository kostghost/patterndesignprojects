﻿using Xrm.ReportUtility.Infrastructure.Transformers;
using Xrm.ReportUtility.Interfaces;
using Xrm.ReportUtility.Models;

namespace Xrm.ReportUtility.Infrastructure
{
    public static class DataTransformerCreator
    {
        //В данном случае можно было бы использовать паттерн Строитель. Создаем сервис с теми свойствами, что нам необходимы.
        //Все они независимы между собой, легко добавить новое свойство.

        //использован паттерн Декоратор. Позволяет независимо добавлять новые фичи.
        //как минус - зависимость от порядка
        //Component - IDataTransformer
        //ConcreateComponent - DataTransformer
        //Decorator - ReportServiceTransformerBase
        //ConcreateDecorator - WithDataReportTransformer, VolumeSumReportTransformer
        public static IDataTransformer CreateTransformer(ReportConfig config)
        {
            IDataTransformer service = new DataTransformer(config);

            if (config.WithData)
            {
                service = new WithDataReportTransformer(service);
            }

            if (config.VolumeSum)
            {
                service = new VolumeSumReportTransformer(service);
            }

            if (config.WeightSum)
            {
                service = new WeightSumReportTransfomer(service);
            }

            if (config.CostSum)
            {
                service = new CostSumReportTransformer(service);
            }

            if (config.CountSum)
            {
                service = new CountSumReportTransformer(service);
            }

            return service;
        }
    }
}