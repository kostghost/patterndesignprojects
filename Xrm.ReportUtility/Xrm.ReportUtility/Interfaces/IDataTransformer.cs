﻿using Xrm.ReportUtility.Models;

namespace Xrm.ReportUtility.Interfaces
{
    //AbstractFactory
    public interface IDataTransformer
    {
        Report TransformData(DataRow[] data);
    }
}