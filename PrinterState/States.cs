﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrinterState
{
    public enum DeviceType {USB , WIFI};

    public interface IState
    {
        void PutTheMoney(PrinterContext context);
        void SelectDevice(PrinterContext context);
        void SelectDocumentOnUsb(PrinterContext context);
        void SelectDocumentOnWiFi(PrinterContext context);
        bool GetCopy(PrinterContext context);
        int GiveChange(PrinterContext context);
    }

    public class PrinterContext
    {
        public IState State { get; set; }

        public int addedMoney = 0;
        public int balance = 0;

        public string selectedDocument;
        public DeviceType selectedDevice; 

        //цена копии
        public int minimumPrice = 10;

        public PrinterContext()
        {
            State = new PutTheMoneyState();
        }

        public void PutTheMoney(int money)
        {
            addedMoney = money;
            State.PutTheMoney(this);
        }

        public void SelectDevice(DeviceType deviceType)
        {
            selectedDevice = deviceType;

            State.SelectDevice(this);
        }

        ///???????? TOO
        public void SelectDocument(string documentName)
        {
            selectedDocument = documentName;

            //Тут нужно сравнить тип State с SelectDocumentOnWiFiState или SelectDocumentOnUsbState
            //State.SelectDocumentOnUsb(this);
            State.SelectDocumentOnWiFi(this);
        }


        public int GetChange()
        {
            return State.GiveChange(this);
        }

        //true if copy is copied :D
        public bool GetCopy()
        {
            return State.GetCopy(this);
        }
    }

    public abstract class StateBase : IState
    {
        public abstract void PutTheMoney(PrinterContext context);
        public abstract bool GetCopy(PrinterContext context);
        public virtual int GiveChange(PrinterContext context)
        {
            int result = context.balance;
            context.balance = 0;
            //context.addedMoney = 0;

            context.State = new PutTheMoneyState();
            if (result > 0)
                Console.WriteLine($"{result}$ returned to you");
            else
                Console.WriteLine("There is 0 on your balance");

            return result;
        }
        public abstract void SelectDevice(PrinterContext context);
        public abstract void SelectDocumentOnUsb(PrinterContext context);
        public abstract void SelectDocumentOnWiFi(PrinterContext context);
    }

    public class PutTheMoneyState : StateBase
    {
        public override void PutTheMoney(PrinterContext context)
        {
            context.balance += context.addedMoney;
            
            if (context.balance >= context.minimumPrice)
            {
                Console.WriteLine($"{context.addedMoney}$ added.");
                context.addedMoney = 0;
                context.State = new SelectDeviceState();
            }
            else
                Console.WriteLine("Not enough money. Give more or get the change");
        }
        //public override int GiveChange(PrinterContext context)
        //{
        //    return base.GiveChange(context);
        //}
        public override bool GetCopy(PrinterContext context)
        {
            throw new Exception("Give me money first");
        }
        public override void SelectDocumentOnWiFi(PrinterContext context)
        {
            throw new Exception("Give me money first");
        }
        public override void SelectDocumentOnUsb(PrinterContext context)
        {
            throw new Exception("Give me money first");
        }
        public override void SelectDevice(PrinterContext context)
        {
            throw new Exception("Give me money first");
        }
    }

    public class SelectDeviceState : StateBase
    {
        public override void PutTheMoney(PrinterContext context)
        {
            throw new Exception("Its unreal to do this while selecting device");
        }
        public override void SelectDevice(PrinterContext context)
        {
            switch (context.selectedDevice)
            {
                case DeviceType.USB:
                    Console.WriteLine("USB device choosed");
                    context.State = new SelectDocumentOnUsbState();
                    break;
                case DeviceType.WIFI:
                    Console.WriteLine("Wi-Fi device choosed");
                    context.State = new SelectDocumentOnWiFiState();
                    break;
            }
        }
        public override bool GetCopy(PrinterContext context)
        {
            throw new Exception("Please select device first");
        }
        public override void SelectDocumentOnUsb(PrinterContext context)
        {
            throw new Exception("Please select device first");
        }
        public override void SelectDocumentOnWiFi(PrinterContext context)
        {
            throw new Exception("Please select device first");
        }
    }

    public class SelectDocumentOnWiFiState : StateBase
    {

        public override bool GetCopy(PrinterContext context)
        {
            throw new Exception("Please select document first");
        }

        public override void PutTheMoney(PrinterContext context)
        {
            throw new Exception("Its unreal to do this while selecting document");
        }

        public override void SelectDevice(PrinterContext context)
        {
            throw new Exception("Its unreal to do this while selecting document");
        }

        public override void SelectDocumentOnUsb(PrinterContext context)
        {
            throw new Exception("Please select document on Wi-Fi, not usb");
        }

        public override void SelectDocumentOnWiFi(PrinterContext context)
        {
            //Тупо сравнваем, есть ли .doc
            if (context.selectedDocument.Contains(".doc"))
            {
                Console.WriteLine("Document selected");
                context.State = new GetCopyState();
            }
        }
    }

    public class SelectDocumentOnUsbState : StateBase
    {

        public override bool GetCopy(PrinterContext context)
        {
            throw new Exception("Please select document first");
        }

        public override void PutTheMoney(PrinterContext context)
        {
            throw new Exception("Its unreal to do this while selecting document");
        }

        public override void SelectDevice(PrinterContext context)
        {
            throw new Exception("Its unreal to do this while selecting document");
        }

        public override void SelectDocumentOnUsb(PrinterContext context)
        {
            if (context.selectedDocument.Contains(".doc"))
            {
                Console.WriteLine("Document selected");
                context.State = new GetCopyState();
            }
        }

        public override void SelectDocumentOnWiFi(PrinterContext context)
        {
            throw new Exception("Please select document on USB, not Wi-Fi");
        }
    }

    public class GetCopyState : StateBase
    {
        public override bool GetCopy(PrinterContext context)
        {
            context.balance -= context.minimumPrice;
            Console.WriteLine($"Document Printed, Your balance now is {context.balance}");
            context.State = new GiveChangeState();
            return true;
        }
        public override void PutTheMoney(PrinterContext context)
        {
            throw new Exception("Its unreal to do this while getting copy");
        }
        public override void SelectDevice(PrinterContext context)
        {
            throw new Exception("Its unreal to do this while getting copy");
        }
        public override void SelectDocumentOnUsb(PrinterContext context)
        {
            throw new Exception("Its unreal to do this while getting copy");
        }
        public override void SelectDocumentOnWiFi(PrinterContext context)
        {
            throw new Exception("Its unreal to do this while getting copy");
        }
    }

    public class GiveChangeState : StateBase
    {
        public override int GiveChange(PrinterContext context)
        {
            return base.GiveChange(context);
        }
        public override bool GetCopy(PrinterContext context)
        {
            throw new Exception("Its unreal to do this while giving change");
        }
        public override void PutTheMoney(PrinterContext context)
        {
            throw new Exception("Its unreal to do this while giving change");
        }
        public override void SelectDevice(PrinterContext context)
        {
            context.State = new SelectDeviceState();
            context.SelectDevice(context.selectedDevice);
        }
        public override void SelectDocumentOnUsb(PrinterContext context)
        {
            throw new Exception("Its unreal to do this while giving change");
        }
        public override void SelectDocumentOnWiFi(PrinterContext context)
        {
            throw new Exception("Its unreal to do this while giving change");
        }
    }
}
