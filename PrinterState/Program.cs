﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrinterState
{
    class Program
    {
        static void Main(string[] args)
        {
            var context = new PrinterContext();

            context.PutTheMoney(100);
            context.SelectDevice(DeviceType.WIFI);
            context.SelectDocument("doc.doc");
            bool copy = context.GetCopy();
            context.SelectDevice(DeviceType.WIFI);
            context.SelectDocument("work.doc");


            copy = context.GetCopy();
            int change = context.GetChange();


            Console.WriteLine();
            try
            {
                var context2 = new PrinterContext();
                context2.PutTheMoney(10);
                context2.SelectDevice(DeviceType.USB);
                context2.SelectDocument("notAdocument");

            }
            catch (Exception ex)
            {
                Console.WriteLine($"context2: {ex.Message}");
            }
            Console.WriteLine();

            Console.WriteLine();
            try
            {
                var context3 = new PrinterContext();
                context3.PutTheMoney(5);
                context3.SelectDevice(DeviceType.USB);
                context3.SelectDocument("notAdocument");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"context3: {ex.Message}");
            }
            Console.WriteLine();

        }
    }
}
