﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailBuilder
{
    class Program
    {
        static void Main(string[] args)
        {
            var mail = new StateMailBuilder()
                .AddReciever("vasya@gmail.com")
                .AddRecieverCopy("vasya@yandex.ru")
                .AddRecieverCopy("aysav@xednay.ur")
                .AddBody("Hello my Dear Friend! \n I'm glad to see you! \n Good bye!")
                .AddSubject("For my friends")       //This line isn't necessary
                .Build;

            Console.WriteLine(mail);

            Console.ReadKey();
        }
    }
}
