﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailBuilder
{
    public interface IMailBuilderReciever
    {
        IMailBuilderBody AddReciever(string reciever);
    }

    public interface IMailBuilderBody
    {
        IMailBuilderSubject AddBody(string body);
        IMailBuilderBody AddRecieverCopy(string reciever);
    }

    public interface IMailBuilderSubject
    {
        IMailBuilderFinal AddSubject(string subject);
        string Build { get; }
    }

    public interface IMailBuilderFinal
    {
        string Build { get; }
    }

    public class StateMailBuilder
    {
        public IMailBuilderBody AddReciever(string reciever)
        {
            Mail _mail = new Mail();
            _mail.Recievers += reciever;
            return new MailBuilderBody(_mail);
        }

        private class MailBuilderBody : IMailBuilderBody
        {
            private readonly Mail _mail;

            public MailBuilderBody(Mail mail)
            {
                _mail = mail;
            }

            public IMailBuilderSubject AddBody(string body)
            {
                _mail.Body = body;
                return new MailBuilderSubject(_mail);
            }

            public IMailBuilderBody AddRecieverCopy(string reciever)
            {
                _mail.Recievers += $", {reciever}";
                return new MailBuilderBody(_mail);
            }
            
            public class MailBuilderSubject : IMailBuilderSubject
            {
                private readonly Mail _mail;

                public MailBuilderSubject(Mail mail)
                {
                    _mail = mail;
                }

                public IMailBuilderFinal AddSubject(string subject)
                {
                    _mail.Subject = subject;
                    return new MailBuilderFinal(_mail);
                }

                public string Build => $"\nto: {_mail.Recievers} \n\n {_mail.Body}";

                public class MailBuilderFinal : IMailBuilderFinal
                {
                    private readonly Mail _mail;

                    public MailBuilderFinal(Mail mail)
                    {
                        _mail = mail;
                    }

                    public string Build => $"\t{_mail.Subject} \nto: {_mail.Recievers} \n\n {_mail.Body}";
                }
            }
        }
    }

    public class Mail
    {
        public string Recievers { get; set; }
        public string Body { get; set; }
        public string Subject { get; set; }
    }
}
