﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Example_04.Homework.Models;

namespace OrmAdapter
{
    class Program
    {
        static void Main(string[] args)
        {
            DataBase dataBase = new DataBase();
            var usersId = new List<int>();

            var ormOne = new FirstOrmAdapter(dataBase, dataBase);
            usersId.Add(ormOne.Create("kostghost", "123", "Konstantin", DateTime.Parse("09.07.1994")));
            Console.WriteLine(ormOne.WriteInfo(usersId[0]));
            ormOne.DeleteUser(usersId[0]);

            var ormTwo = new SecondOrmAdapter();
            usersId.Add(ormTwo.Create("stalkerugas", "321", "Dmitriy", DateTime.Parse("10.12.1994")));
            Console.WriteLine(ormTwo.WriteInfo(usersId[1]));
            ormTwo.DeleteUser(usersId[1]);
            Console.Read();
        }
    }
}
