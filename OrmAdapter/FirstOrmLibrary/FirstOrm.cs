﻿using Example_04.Homework.FirstOrmLibrary;
using Example_04.Homework.Models;
using Example_04.Homework.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrmAdapter.FirstOrmLibrary
{
    public class FirstOrmUser<TDbEntity> : IFirstOrm<TDbEntity> where TDbEntity : IDbEntity
    {
        DataBase database;

        public FirstOrmUser(DataBase _database)
        {
            database = _database;
        }

        public void Create(TDbEntity entity)
        {
            database.ormEntities.Add(entity.Id, entity);
        }

        public TDbEntity Read(int id)
        {
            return (TDbEntity)database.ormEntities[id];
        }

        public void Update(TDbEntity entity)
        {
            database.ormEntities[entity.Id] = entity;
        }

        public void Delete(TDbEntity entity)
        {
            database.ormEntities.Remove(entity.Id);
        }
    }
}
