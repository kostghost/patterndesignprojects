﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Example_04.Homework.Models.Interfaces;
using Example_04.Homework.FirstOrmLibrary;
using Example_04.Homework.SecondOrmLibrary;
using OrmAdapter.FirstOrmLibrary;
using Example_04.Homework.Models;

namespace OrmAdapter
{
    public class FirstOrmAdapter: IOrmTarget
    {
        private Random rnd = new Random();
        private FirstOrmUser<DbUserEntity> firstOrmUser;
        private FirstOrmUser<DbUserInfoEntity> firstOrmUserInfo;
        DataBase dataBaseUsers;
        DataBase dataBaseUserInfo;

        public FirstOrmAdapter(DataBase _dataBaseUsers, DataBase _dataBaseUserInfo)
        {
            firstOrmUser = new FirstOrmUser<DbUserEntity>(_dataBaseUsers);
            dataBaseUsers = _dataBaseUsers; //костыль, пожалуй
            firstOrmUserInfo = new FirstOrmUser<DbUserInfoEntity>(_dataBaseUserInfo);
            dataBaseUserInfo = _dataBaseUserInfo; //костыль, пожалуй
        }

        public int Create(string login, string passHash, string name, DateTime birthday)
        {
            var entity = new DbUserEntity();
            var entityInfo = new DbUserInfoEntity();

            entity.Id = rnd.Next();     //Нет проверки на совпадения C:
            entity.Login = login;
            entity.PasswordHash = passHash;
            entity.UserInfoId = rnd.Next(); //Нет проверки на совпадения C:

            entityInfo.Id = entity.UserInfoId;
            entityInfo.Name = name;
            entityInfo.Birthday = birthday;

            firstOrmUser.Create(entity);
            firstOrmUserInfo.Create(entityInfo);

            return entity.Id;
        }

        public void Update(int id, string login, string passHash, string name, DateTime birthday)
        {
            DeleteUser(id);
            Create(login, passHash, name, birthday);
        }

        public void DeleteUser(int id)
        {
            //Опять же нет проверки
            firstOrmUser.Delete((DbUserEntity)dataBaseUsers.ormEntities[id]);
        }

        public string WriteInfo(int id)
        {
            var entity = (DbUserEntity)dataBaseUsers.ormEntities[id];
            var idInfo = entity.UserInfoId;
            var entityInfo = (DbUserInfoEntity)dataBaseUserInfo.ormEntities[idInfo];
            
            return String.Format("Login: {0}, Name: {1}, Birthday: {2} ", entity.Login, entityInfo.Name, entityInfo.Birthday);
        }
    }
}
