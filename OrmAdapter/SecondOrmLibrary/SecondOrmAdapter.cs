﻿using Example_04.Homework.Models;
using OrmAdapter.SecondOrmLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrmAdapter
{
    class SecondOrmAdapter : IOrmTarget
    {
        Random rnd = new Random();
        SecondOrm orm = new SecondOrm();

        public int Create(string login, string passHash, string name, DateTime birthday)
        {
            var entity = new DbUserEntity();
            var entityInfo = new DbUserInfoEntity();

            entity.Id = rnd.Next();     //Нет проверки на совпадения C:
            entity.Login = login;
            entity.PasswordHash = passHash;
            entity.UserInfoId = rnd.Next(); //Нет проверки на совпадения C:

            entityInfo.Id = entity.UserInfoId;
            entityInfo.Name = name;
            entityInfo.Birthday = birthday;

            orm.Context.Users.Add(entity);
            orm.Context.UserInfos.Add(entityInfo);
                         
            return entity.Id;
        }

        public void DeleteUser(int id)
        {
            //ищем entity, в котором содержится наш id
            var entity = orm.Context.Users.FirstOrDefault(item => item.Equals(id));

            orm.Context.Users.Remove(entity); 
        }

        public void Update(int id, string login, string passHash, string name, DateTime birthday)
        {
            DeleteUser(id);
            Create(login, passHash, name, birthday);
        }

        public string WriteInfo(int id)
        {
            
            var entity = orm.Context.Users.FirstOrDefault(item => item.Id.Equals(id));
            var entityInfo = orm.Context.UserInfos.FirstOrDefault(item => item.Id.Equals(entity.UserInfoId));
            
            return String.Format("Login: {0}, Name: {1}, Birthday: {2} ", entity.Login, entityInfo.Name, entityInfo.Birthday);
        }
    }
}
