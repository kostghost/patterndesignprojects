﻿using Example_04.Homework.Models;
using Example_04.Homework.SecondOrmLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrmAdapter.SecondOrmLibrary
{
    public class SecondOrm : ISecondOrm
    {
        public ISecondOrmContext Context { get; }

        public SecondOrm()
        {
            Context = new SecondOrmContext();
        }

    }

    public class SecondOrmContext : ISecondOrmContext
    {
        public HashSet<DbUserEntity> Users { get; }
        public HashSet<DbUserInfoEntity> UserInfos { get; }

        public SecondOrmContext()
        {
            Users = new HashSet<DbUserEntity>();
            UserInfos = new HashSet<DbUserInfoEntity>();

        }
    }
}
