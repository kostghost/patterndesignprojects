﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Example_04.Homework.Models.Interfaces;

namespace OrmAdapter
{
    public interface IOrmTarget
    {
        int Create(string login, string passHash, string name, DateTime birthday); //return id
        void Update(int id, string login, string passHash, string name, DateTime birthday);
        void DeleteUser(int id);
        string WriteInfo(int id);
    }

}
