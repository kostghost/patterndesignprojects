﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatDecorator.Decorators
{
    public class ChatDecoratorBase : IChat
    {
        protected readonly IChat Decoratee;

        protected ChatDecoratorBase(IChat chat)
        {
            Decoratee = chat;
        }

        public IMessage GetMessage()
        {
            var msg = Decoratee.GetMessage();
            if (msg == null)
                return msg;
            return OnAfterGetMessage(msg);
        }

        public bool SendMessage(IMessage message)
        {
            var msg = OnBeforeSendMessage(message); //тут декорируем перед отправкой
            return Decoratee.SendMessage(msg);
        }

        protected virtual IMessage OnBeforeSendMessage(IMessage input)
        {
            return input;
        }

        protected virtual IMessage OnAfterGetMessage(IMessage input)
        {
            return input;
        }
        
    }

    public class ChatAnonymousDecorator : ChatDecoratorBase
    {
        private const int SECRET_KEY = 397;

        public ChatAnonymousDecorator(IChat chat) : base(chat)
        { }

        private string EncryptName(string str)
        {
            StringBuilder res = new StringBuilder();
            foreach(char c in str)
            {
                res.Append((int)c + SECRET_KEY);
            }
            return res.ToString();
        }

        protected override IMessage OnBeforeSendMessage(IMessage input)
        {
            var annonymusMessage = input; //Вообще, тут лучше сделать копию. Но заморачиваться не стал.
            annonymusMessage.Author = EncryptName(annonymusMessage.Author);
            annonymusMessage.Destination = EncryptName(annonymusMessage.Destination);
            return base.OnBeforeSendMessage(annonymusMessage);
        }
    }

    public class ChatEncryptingDecorator : ChatDecoratorBase
    {
        private const string ENCRYPTING_TAG = "<<encrypted>>";

        public ChatEncryptingDecorator(IChat chat) : base(chat)
        { }
        
        private string EncryptString(string str)
        {
            return ENCRYPTING_TAG + str;
        }

        private string DecryptString(string str)
        {
            return str.Replace(ENCRYPTING_TAG, "");
        }

        protected override IMessage OnBeforeSendMessage(IMessage input)
        {
            var encMessage = input;  //Вообще, тут бы сделать копию. Но заморачиваться не стал.
            encMessage.Author = EncryptString(encMessage.Author);
            encMessage.Body = EncryptString(encMessage.Body);
            encMessage.Destination = EncryptString(encMessage.Destination);

            return base.OnBeforeSendMessage(encMessage);
        }

        protected override IMessage OnAfterGetMessage(IMessage input)
        {
            var result = input; //Вообще, тут бы сделать копию. Но заморачиваться не стал.

            result.Author = DecryptString(result.Author);
            result.Body = DecryptString(result.Body);
            result.Destination = DecryptString(result.Destination);

            return base.OnAfterGetMessage(result);
        }

    }

}
