﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatDecorator.Decorators
{
    class ChatDecoratorBuilder
    {
        private IChat _chat;

        public ChatDecoratorBuilder(IChat chat)
        {
            _chat = chat;
        }

        public ChatDecoratorBuilder WithEncrypting()
        {
            _chat  = new ChatEncryptingDecorator(_chat);
            return this;
        }

        public ChatDecoratorBuilder WithAnnonymus()
        {
            _chat = new ChatAnonymousDecorator(_chat);
            return this;
        }

        public IChat Build()
        {
            return _chat;
        }

    }
}
