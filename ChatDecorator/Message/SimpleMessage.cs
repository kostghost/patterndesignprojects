﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatDecorator.Message
{
    public class SimpleMessage : IMessage
    {
        public string Author { get; set; }
        public string Destination { get; set; }
        public string Body { get; set; }

        public SimpleMessage(string author, string destination, string body)
        {
            Author = author;
            Destination = destination;
            Body = body;
        }
    }
}
