﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatDecorator
{
    public interface IMessage
    {
        string Author { get; set; }
        string Destination { get; set; }
        string Body { get; set; }
    }
}
