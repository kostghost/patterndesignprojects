﻿using ChatDecorator.Chat;
using ChatDecorator.Decorators;
using ChatDecorator.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatDecorator
{
    class Program
    {
        public static void WriteMessage(IMessage msg)
        {
            Console.WriteLine("From: {0}  To: {1} \n  {2}", msg.Author, msg.Destination, msg.Body);
        }

        static void Main(string[] args)
        {
           //Обычный чат
           IChat chat = new SimpleChat();

            chat = new ChatDecoratorBuilder(chat)
                .WithEncrypting()       //Чат с шифрованием
                .WithAnnonymus()        //Чат с анинимностью
                .Build();
                
            chat.SendMessage(new SimpleMessage("Vasya", "Kostya", "Hello!"));
            chat.SendMessage(new SimpleMessage("Vasya", "Kostya", "How are you?"));

            IMessage recievedMessage;
            //получаем все сообщения 
            while (true)
            {
                recievedMessage = chat.GetMessage();
                if (recievedMessage == null)
                    break;
                WriteMessage(recievedMessage);
            } 
            Console.Read();
        }
    }
}
