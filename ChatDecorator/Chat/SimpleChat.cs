﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatDecorator.Chat
{
    class SimpleChat : IChat
    {
        public IMessage GetMessage()
        {
            if (MessagesQueue.messages.Count > 0)
            {
                return MessagesQueue.messages.Dequeue();
            }

            return null;
        }

        public bool SendMessage(IMessage message)
        {
            if (MessagesQueue.messages != null)
            {
                MessagesQueue.messages.Enqueue(message);
                return true;
            }
            return false;
        }
    }
}
