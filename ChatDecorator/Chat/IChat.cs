﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatDecorator
{
    public interface IChat
    {
        bool SendMessage(IMessage message);
        IMessage GetMessage();
    }
}
