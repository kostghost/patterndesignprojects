﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactory
{
    class Program
    {
        static void Main(string[] args)
        {
            var audiFactory = new AudiFactory();
            var bmwFactory = new BmwFactory();

            PrintCarComponents(audiFactory);
            PrintCarComponents(bmwFactory);
        }

        public static void PrintCarComponents(ICarFactory factory)
        {
            Console.WriteLine($"\t{factory.FactoryName} car components:");

            var body = factory.CreateBody();
            Console.WriteLine(body.Type);

            var engine = factory.CreateEngine();
            Console.WriteLine(engine.Type);

            var door = factory.CreateDoors();
            Console.WriteLine(door.Type);

            var interior = factory.CreateInterior();
            Console.WriteLine(interior.Type);

            Console.WriteLine();
        }
    }
}
