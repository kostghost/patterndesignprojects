﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactory
{
    public interface ICarFactory
    {
        string FactoryName { get; }
        IBody CreateBody();
        IEngine CreateEngine();
        IDoors CreateDoors();
        IInterior CreateInterior();
    }

    public interface IBody
    {
        string Type { get; }
    }

    public interface IEngine
    {
        string Type { get; }
    }

    public interface IDoors
    {
        string Type { get; }
    }

    public interface IInterior
    {
        string Type { get; }
    }

    #region BMW
    public class BmwFactory : ICarFactory
    {
        public string FactoryName => "BMW factory";

        public IDoors CreateDoors()
        {
            return new BmwDoor();
        }

        public IBody CreateBody()
        {
            return new BmwBody();
        }

        public IInterior CreateInterior()
        {
            return new BmwInterior();
        }

        public IEngine CreateEngine()
        {
            return new BmwEngine();
        }

    }

    public class BmwBody : IBody
    {
        public string Type => "BMW Body";
    }

    public class BmwEngine : IEngine
    {
        public string Type => "BMW Engine";
    }

    public class BmwDoor : IDoors
    {
        public string Type => "BMW Door";
    }

    public class BmwInterior : IInterior
    {
        public string Type => "BMW Interior";
    }
    #endregion

    #region AUDI
    public class AudiFactory : ICarFactory
    {
        public string FactoryName =>  "AUDI factory";

        public IBody CreateBody()
        {
            return new AudiBody();
        }

        public IEngine CreateEngine()
        {
            return new AudiEngine();
        }

        public IDoors CreateDoors()
        {
            return new AudiDoor();
        }

        public IInterior CreateInterior()
        {
            return new AudiInterior();
        }
    }

    public class AudiBody : IBody
    {
        public string Type => "AUDI Body";
    }

    public class AudiEngine : IEngine
    {
        public string Type => "AUDI Engine";
    }

    public class AudiDoor : IDoors
    {
        public string Type => "AUDI Door";
    }

    public class AudiInterior : IInterior
    {
        public string Type => "AUDI Interior";
    }

    #endregion
}
